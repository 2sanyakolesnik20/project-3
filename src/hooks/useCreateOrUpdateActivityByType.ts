// hooks
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';

// thunks
import { updateActivityByTypeThunk, createActivityByTypeThunk } from '../lib/redux/thunk';

// selectors
import { authTokenSelector } from '../lib/redux/selectors';

// types
import { RecordRequestType, ResponseRecordType } from '../types';

type ArgTypes = {
    activity: ResponseRecordType | null
    types: RecordRequestType
};

type ReturnFuncType = ({ activity, types }: ArgTypes) => void;

export const useCreateOrUpdateActivityByType = (): ReturnFuncType => {
    const authToken = useAppSelector(authTokenSelector);
    const dispatch = useAppDispatch();

    return ({ activity, types }) => {
        if (activity?.value !== types.record && activity?.hash === '0') {
            return dispatch(createActivityByTypeThunk(types, authToken));
        }
        if (activity?.hash && activity.hash !== '0') {
            dispatch(updateActivityByTypeThunk(types, authToken, activity.hash));
        }
    };
};
