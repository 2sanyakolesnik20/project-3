/* eslint-disable node/no-missing-import */
/* eslint-disable import/no-unresolved */
// core
import { UseFormRegisterReturn } from 'react-hook-form/dist/types/form';

// types
import { ValueType } from '../../types';

type InputPropsType = {
    tag?: string;
    type?: string;
    register: UseFormRegisterReturn;
    label?: string;
    classNameLabel?: string
    value?: string | number
    id?: string
    htmlFor?: string
    checkedValue?: ValueType
    setMlOfWater?: React.Dispatch<React.SetStateAction<number>>
};

export const QuestionsInput: React.FC<InputPropsType> = ({
    type, register, label, classNameLabel, value, id, htmlFor, checkedValue, setMlOfWater,
}) => {
    const input = (
        <input
            onClick = { () => {
                if (setMlOfWater) {
                    setMlOfWater(typeof value === 'number' ? value : 0);
                }
            } }
            defaultChecked = { checkedValue === value }
            id = { id }
            type = { type }
            { ...register }
            value = { value } />
    );

    return (
        <>
            { input }
            <label htmlFor = { htmlFor } className = { classNameLabel } >
                { label }
            </label>
        </>
    );
};

QuestionsInput.defaultProps = {
    type: 'text',
    tag:  'input',
};
