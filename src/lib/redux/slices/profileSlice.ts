/* eslint-disable no-param-reassign */
// core
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import { ProfileResponseType } from '../../../types';

export type InitialProfileSliceStateType = {
    userInfo: ProfileResponseType | null
};

const initialState: InitialProfileSliceStateType = {
    userInfo: null,
};

const profileSlice = createSlice({
    name:     'profile',
    initialState,
    reducers: {
        getUserProfileInfo: (state, action: PayloadAction<ProfileResponseType | null>) => {
            state.userInfo = action.payload;
        },
    },
});

export const { getUserProfileInfo } = profileSlice.actions;
export default profileSlice.reducer;
