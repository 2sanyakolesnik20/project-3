// core
import { FC } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

// hooks
import { useForm } from 'react-hook-form';
import { useAppDispatch } from '../../../lib/redux/init/store';

// components
import { FormsInput } from '../../../elements/customInput';

// thunks
import { loginRequestThunk } from '../../../lib/redux/thunk';

// types
import { LoginRequestType } from '../../../types';

// styles
import st from '../../../bus/user/components/login/styles/index.module.scss';

// helpers
import { book } from '../../../navigation/book';

// schema
import { schema } from './config';

export const LoginForm: FC = () => {
    const dispatch = useAppDispatch();
    const navigate = useNavigate();

    const { register, handleSubmit, formState } = useForm<LoginRequestType>({
        mode:     'onChange',
        resolver: yupResolver(schema),
    });

    const submit = handleSubmit((payload: LoginRequestType) => {
        dispatch(loginRequestThunk({ payload, navigate }));
    });

    return (
        <form onSubmit = { submit } className = { st.content }>
            <h1>Добро пожаловать!</h1>
            <FormsInput
                classNameLabel = { st.inputRow }
                label = 'Электропочта'
                type = 'email'
                placeholder = 'Введите свою электропочту'
                error = { formState.errors.email }
                register = { register('email') } />
            <FormsInput
                classNameLabel = { st.inputRow }
                label = 'Пароль'
                type = 'password'
                placeholder = 'Введите свой пароль'
                error = { formState.errors.password }
                register = { register('password') } />
            <div>
                <button>Войти в систему</button>
                <div className = { st.loginLink }>
                    <span>Если у вас нет аккаунта, пожалуйста </span>
                    <Link to = { book.registration.url }> зарегистрируйтесь</Link>
                </div>
            </div>
        </form>
    );
};

