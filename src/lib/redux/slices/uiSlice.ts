/* eslint-disable no-param-reassign */
// core
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export type InitialUISliceStateType = {
    errorMessage: string | null,
    isFetching: boolean
};

const initialState: InitialUISliceStateType = {
    errorMessage: '',
    isFetching:   false,
};

const uiSlice = createSlice({
    name:     'ui',
    initialState,
    reducers: {
        setIsFetching: (state, action: PayloadAction<boolean>) => {
            state.isFetching = action.payload;
        },
        setErrorMessage: (state, action: PayloadAction<string | null>) => {
            state.errorMessage = action.payload;
        },
    },
});

export const { setErrorMessage, setIsFetching } = uiSlice.actions;
export default uiSlice.reducer;
