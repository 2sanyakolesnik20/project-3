/* eslint-disable no-template-curly-in-string */
// core
import * as yup from 'yup';

// types
import { ProfileFormType } from '../../../types';

const minLength = 'Минимальная длина ${min}';
const maxLength = 'Максимальная длина ${max}';

export const schema: yup.SchemaOf<ProfileFormType> = yup.object().shape({
    fname:    yup.string().required('*').min(3, minLength).max(12, maxLength),
    lname:    yup.string().required('*').min(3, minLength).max(12, maxLength),
    email:    yup.string().required('*').email('Введите корректную электронную почту'),
    password: yup.string().required('*').min(8, minLength).max(12, maxLength),
    age:      yup.number().required('*'),
    sex:      yup.string().required('*'),
    height:   yup.number().required('*'),
    weight:   yup.number().required('*'),
});
