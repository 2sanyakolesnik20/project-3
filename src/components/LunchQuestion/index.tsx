// core
import { FC } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';
import { useAppSelector } from '../../lib/redux/init/store';

// components
import { QuestionsInput } from '../../elements/customInput';

// selectors
import { lunchActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionSelector/styles/index.module.scss';

export const LunchQuestion: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const lunchActivity = useAppSelector(lunchActivitySelector);
    const { submit }  = useSubmit({ handleSubmit, activity: lunchActivity });

    useGetActivityByType({ activity: lunchActivity, types: RecordKinds.LUNCH });
    useResetForm({ activity: lunchActivity, reset });

    return (
        <form onSubmit = { submit } className = { st.question }>
            <h1> Ты сегодня обедал? </h1>
            <div className = { st.answers }>
                <QuestionsInput
                    checkedValue = { lunchActivity?.value }
                    htmlFor = 'none'
                    id = 'none'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('lunch') }
                    value = 'none'
                    label = 'Я не обедал' />
                <QuestionsInput
                    checkedValue = { lunchActivity?.value }
                    htmlFor = 'light'
                    id = 'light'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('lunch') }
                    value = 'light'
                    label = 'У меня был легкий обед' />
                <QuestionsInput
                    checkedValue = { lunchActivity?.value }
                    htmlFor = 'heavy'
                    id = 'heavy'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('lunch') }
                    value = 'heavy'
                    label = 'Я очень плотно покушал' />
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};
