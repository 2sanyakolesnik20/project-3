// core
import { FC } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';
import { useAppSelector } from '../../lib/redux/init/store';

// selectors
import { stepsActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionInput/styles/index.module.scss';

export const StepsQuestions: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const stepsActivity = useAppSelector(stepsActivitySelector);
    const { submit }  = useSubmit({ handleSubmit, activity: stepsActivity });

    useGetActivityByType({ activity: stepsActivity, types: RecordKinds.STEPS });
    useResetForm({ activity: stepsActivity, reset });

    return (
        <form onSubmit = { submit } className = { st.question }>
            <h1>Сколько шагов ты сегодня прошел?</h1>
            <div className = { st.inputRow }>
                <input
                    defaultValue = { typeof stepsActivity?.value === 'number' ? stepsActivity?.value : 0 }
                    type = 'number'
                    placeholder = 'Введите свое число'
                    { ...register('steps') } />
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};

