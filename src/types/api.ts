//* profile response and request types *//

export type ProfileFormType = {
    fname: string;
    lname: string;
    email: string;
    password: string;
    age: number;
    sex: string,
    height: number;
    weight: number;
};

export type ProfileRequestType = Omit<ProfileFormType, 'password'>;

export type ProfileResponseType = ProfileRequestType & {
    hash: string;
    created: string;
};

//* record request and response types *//
export type ValueType = number | string | boolean;

export type RecordRequestType = {
    type: string;
    record: ValueType;
};

export type ResponseRecordType = {
    hash: string;
    value: ValueType;
};

export enum RecordKinds {
    BREAKFAST = 'breakfast',
    LUNCH = 'lunch',
    DINNER = 'dinner',
    STEPS = 'steps',
    FRUITS = 'fruits',
    VEGETABLES = 'vegetables',
    JUNK = 'junk',
    WATER = 'water',
    SLEEP = 'sleep',
    COFFEE = 'coffee',
}

export enum Sex {
    MALE = 'm',
    FEMALE = 'f',
}

//* login Request *//

export type LoginRequestType = Pick<ProfileFormType, 'email' | 'password'>;

export type TokenResponseType =  string | null;
