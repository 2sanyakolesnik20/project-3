// core
import { RootState } from '../init/store';

export const currentActivityScoreSelector = (state: RootState) => state.activity.currentActivityScore;
export const breakfastActivitySelector = (state: RootState) => state.activity.breakfastActivity;
export const lunchActivitySelector = (state: RootState) => state.activity.lunchActivity;
export const dinnerActivitySelector = (state: RootState) => state.activity.dinnerActivity;
export const stepsActivitySelector = (state: RootState) => state.activity.stepsActivity;
export const fruitsActivitySelector = (state: RootState) => state.activity.fruitsActivity;
export const vegetablesActivitySelector = (state: RootState) => state.activity.vegetablesActivity;
export const junkActivitySelector = (state: RootState) => state.activity.junkActivity;
export const waterActivitySelector = (state: RootState) => state.activity.waterActivity;
export const sleepActivitySelector = (state: RootState) => state.activity.sleepActivity;
export const coffeeActivitySelector = (state: RootState) => state.activity.coffeeActivity;
