// core
import { FC } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';
import { useAppSelector } from '../../lib/redux/init/store';

// components
import { QuestionsInput } from '../../elements/customInput';

// selectors
import { dinnerActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionSelector/styles/index.module.scss';

export const DinnerQuestion: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const dinnerActivity = useAppSelector(dinnerActivitySelector);
    const { submit }  = useSubmit({ handleSubmit, activity: dinnerActivity });

    useGetActivityByType({ activity: dinnerActivity, types: RecordKinds.DINNER });
    useResetForm({ activity: dinnerActivity, reset });

    return (
        <form onSubmit = { submit } className = { st.question }>
            <h1> Ты сегодня ужинал? </h1>
            <div className = { st.answers }>
                <QuestionsInput
                    checkedValue = { dinnerActivity?.value }
                    htmlFor = 'none'
                    id = 'none'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('dinner') }
                    value = 'none'
                    label = 'Я не ужинал' />
                <QuestionsInput
                    checkedValue = { dinnerActivity?.value }
                    htmlFor = 'light'
                    id = 'light'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('dinner') }
                    value = 'light'
                    label = 'У меня был легкий ужин' />
                <QuestionsInput
                    checkedValue = { dinnerActivity?.value }
                    htmlFor = 'heavy'
                    id = 'heavy'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('dinner') }
                    value = 'heavy'
                    label = 'Я очень плотно покушал' />
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};
