export enum ToastOptions {
    info = 'info',
    success = 'success',
    warning = 'warning',
    error = 'error',
}
