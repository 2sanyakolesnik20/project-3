// core
import { Id, toast, ToastContent } from 'react-toastify';

// types
import { ToastOptions } from '../types';

// constants
import { toastOptions } from '../constants';

export type CreateToastersType = <O extends ToastOptions>(
    typeOfToasts: O,
    message: ToastContent
) => Id;

export const createToasters: CreateToastersType = (typeOfToast, message) => {
    return toast[ typeOfToast ](message, toastOptions);
};

