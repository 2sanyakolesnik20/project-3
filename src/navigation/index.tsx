/* eslint-disable react/no-children-prop */
// Core
import { FC } from 'react';
import {
    Route, Routes, Navigate,
} from 'react-router-dom';

// hooks
import { useError, useInterceptors } from '../hooks';

// Routes
import { book } from './book';

export const RoutesComponent: FC = () => {
    useError();
    useInterceptors();

    const routesJSX = Object
        .values(book)
        .map(({ url, page: Page }) => (
            <Route
                key = { url } path = { url }
                element = { <Page /> } />
        ));

    return (
        <>
            <Routes>
                { routesJSX }
                <Route path = '*'  element = { <Navigate to = { book.login.url } replace /> } />
            </Routes>
        </>
    );
};
