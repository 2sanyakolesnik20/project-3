// book
import { book } from '../../navigation/book';

// styles
import st from '../../bus/tracker/components/dashboard/styles/index.module.scss';

// types
export type QuestionCardsType = {
    title: string
    desc: string
    url: string
    className: string
};

export const mockQuestionCards = (): QuestionCardsType[] => {
    return (
        [
            {
                title:     'Добавить завтрак',
                desc:      'Хороший завтрак очень важен',
                url:       book.breakfast.url,
                className: `${st.category0}`,
            },
            {
                title:     'Добавить обед',
                desc:      'Успешные люди обедают',
                url:       book.lunch.url,
                className: `${st.category1}`,
            },
            {
                title:     'Добавить ужин',
                desc:      'Лучше не ужинать вообще',
                url:       book.dinner.url,
                className: `${st.category2}`,
            },
            {
                title:     'Добавить активность',
                desc:      'Пешие прогулки это минимум',
                url:       book.steps.url,
                className: `${st.category3}`,
            },
            {
                title:     'Добавить фрукты',
                desc:      'Фрукты подымают настроение',
                url:       book.fruits.url,
                className: `${st.category4}`,
            },
            {
                title:     'Добавить овощи',
                desc:      'Овощи очень важны',
                url:       book.vegetables.url,
                className: `${st.category5}`,
            },
            {
                title:     'Добавить фастфуд',
                desc:      'Эта еда очень вредная',
                url:       book.junk.url,
                className: `${st.category6}`,
            },
            {
                title:     'Добавить воду',
                desc:      'Вода это жизнь',
                url:       book.water.url,
                className: `${st.category7}`,
            },
            {
                title:     'Добавить сон',
                desc:      'Спать нужно всем',
                url:       book.sleep.url,
                className: `${st.category8}`,
            },
            {
                title:     'Добавить кофе',
                desc:      'Можно и без него',
                url:       book.coffee.url,
                className: `${st.category9}`,
            },
        ]
    );
};
