// core
import { AnyAction } from '@reduxjs/toolkit';
import { ThunkDispatch } from 'redux-thunk';

// actions
import {
    InitialActivitySliceStateType,
    setBreakfastActivity,
    setCoffeeActivity,
    setDinnerActivity,
    setFruitsActivity,
    setJunkActivity,
    setLunchActivity,
    setSleepActivity,
    setStepsActivity,
    setVegetablesActivity,
    setWaterActivity,
} from '../lib/redux/slices';

// types
import { RecordKinds, ResponseRecordType } from '../types';

export const checkActivityType = (
    type: string, response: ResponseRecordType,
    dispatch: ThunkDispatch<{
        activity: InitialActivitySliceStateType;
    }, unknown, AnyAction>,
) => {
    if (type === RecordKinds.BREAKFAST) {
        return dispatch(setBreakfastActivity(response));
    }
    if (type === RecordKinds.LUNCH) {
        return dispatch(setLunchActivity(response));
    }
    if (type === RecordKinds.DINNER) {
        return dispatch(setDinnerActivity(response));
    }
    if (type === RecordKinds.STEPS) {
        return dispatch(setStepsActivity(response));
    }
    if (type === RecordKinds.FRUITS) {
        return dispatch(setFruitsActivity(response));
    }
    if (type === RecordKinds.VEGETABLES) {
        return dispatch(setVegetablesActivity(response));
    }
    if (type === RecordKinds.JUNK) {
        return dispatch(setJunkActivity(response));
    }
    if (type === RecordKinds.WATER) {
        return dispatch(setWaterActivity(response));
    }
    if (type === RecordKinds.SLEEP) {
        return dispatch(setSleepActivity(response));
    }
    if (type === RecordKinds.COFFEE) {
        return dispatch(setCoffeeActivity(response));
    }
};
