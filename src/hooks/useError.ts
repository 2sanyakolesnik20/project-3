// hooks
import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';

// selectors && actions
import { errorMessageSelector } from '../lib/redux/selectors';
import { setErrorMessage } from '../lib/redux/slices';

// types
import { ToastOptions } from '../types';

// options
import { createToasters } from '../helpers';

export const useError = (): void => {
    const errorMessage = useAppSelector(errorMessageSelector);
    const dispatch = useAppDispatch();

    useEffect((): void => {
        if (errorMessage) {
            createToasters(ToastOptions.error, errorMessage);
            dispatch(setErrorMessage(null));
        }
    }, [errorMessage]);
};
