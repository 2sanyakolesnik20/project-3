// Core
import { AnyAction, configureStore } from '@reduxjs/toolkit';
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux';
import { ThunkAction, ThunkDispatch } from 'redux-thunk';

// slices
import uiSlice from '../slices/uiSlice';
import authSlice from '../slices/authSlice';
import profileSlice from '../slices/profileSlice';
import activitySlice from '../slices/activitySlice';

export const store = configureStore({
    reducer: {
        ui:       uiSlice,
        auth:     authSlice,
        profile:  profileSlice,
        activity: activitySlice,
    },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = TDispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;
export type AppThunk<ReturnType = void> = ThunkAction<ReturnType, RootState, unknown, AnyAction>;
export type TDispatch = ThunkDispatch<RootState, void, AnyAction>;

