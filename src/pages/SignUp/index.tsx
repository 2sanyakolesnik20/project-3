// core
import { FC } from 'react';

// components
import { SignUpForm } from '../../components/forms/SignUpForm';

// styles
import st from '../../bus/user/components/registration/styles/index.module.scss';

export const SignUp: FC = () => {
    return (
        <section className = { st.registration }>
            <div className = { st.left }>
                <SignUpForm />
            </div>
            <div className = { st.right }></div>
        </section>
    );
};
