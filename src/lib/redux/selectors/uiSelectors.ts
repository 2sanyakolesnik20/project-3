// core
import { RootState } from '../init/store';

export const errorMessageSelector = (state: RootState) => state.ui.errorMessage;
export const isFetchingSelector = (state: RootState) => state.ui.isFetching;
