// Core
import { FC, ReactElement, useEffect } from 'react';
import cx from 'classnames';
import {
    Link, matchPath, Navigate, useLocation, useNavigate,
} from 'react-router-dom';

// Elements
import { Spinner } from '../../elements/spinner';

// Book
import { book } from '../../navigation/book';

// styles
import st from './styles/index.module.scss';
import stUser from '../../elements/user/styles.module.scss';

// Hooks
import { useAppDispatch, useAppSelector } from '../../lib/redux/init/store';

// actions
import { setAuthToken } from '../../lib/redux/slices';

// selectors && thunks
import {
    authTokenSelector, currentActivityScoreSelector, isFetchingSelector, userProfileInfoSelector,
} from '../../lib/redux/selectors';
import { getProfileRequestThunk, logOutRequestThunk } from '../../lib/redux/thunk';

// types
import { Sex } from '../../types';

// helpers
import { resetAuthData } from '../../helpers';

export const Base: FC<IPropTypes> = ({ children, center, disabledWidget }) => {
    const { pathname } = useLocation();
    const navigate = useNavigate();
    const dispatch = useAppDispatch();
    const authToken = useAppSelector(authTokenSelector);
    const userInfo = useAppSelector(userProfileInfoSelector);
    const currentActivityScore = useAppSelector(currentActivityScoreSelector);
    const isFetching = useAppSelector(isFetchingSelector);
    const token = localStorage.getItem('token');

    if (!authToken && !token) {
        return <Navigate to = { book.login.url } />;
    }

    if (!authToken && token) {
        dispatch(setAuthToken(token));
    }

    const logOutHandler = () => {
        dispatch(logOutRequestThunk(authToken));
        resetAuthData({ dispatch, navigate });
    };

    const isExact = matchPath(book.root.url, pathname);

    // TODO; необходимо заменить на получаемые с сервера данные
    const score = currentActivityScore;

    // TODO; необходимо динамически менять значения в зависимости от выбранного при регистрации пола
    const avatarCX = cx([
        st.sidebar, {
            [ st.male ]:   userInfo?.sex === Sex.MALE,
            [ st.female ]: userInfo?.sex === Sex.FEMALE,
        },
    ]);

    const contentCX = cx(st.content, {
        [ st.center ]: center,
    });

    const loaderCX = isFetching && (
        <Spinner isLoading = { isFetching } />
    );

    const widgetJSX = score !== null && !disabledWidget && (
        <div className = { st.widget }>
            <span className = { st.title }>Life Score</span>
            <div className = { st.module }>
                <span
                    className = { st.score } style = { {
                        bottom: `${score < 100 ? score : 100}%`,
                    } }>{ score }</span>
                <div className = { st.progress }>
                    <div className = { st.fill } style = { { height: `${score}%` } } />
                </div>
                <span className = { cx([st.label, st.level1]) }>Off Track</span>
                <span className = { cx([st.label, st.level2]) }>Imbalanced</span>
                <span className = { cx([st.label, st.level3]) }>Balanced</span>
                <span className = { cx([st.label, st.level4]) }>Healthy</span>
                <span className = { cx([st.label, st.level5]) }>Perfect Fit</span>
            </div>
        </div>
    );

    const homeLinkJSX = !isExact && (
        <Link to = { book.root.url } className = { st.homeLink }>На главную</Link>
    );

    useEffect(() => {
        if (authToken && !userInfo) {
            dispatch(getProfileRequestThunk(authToken));
        }
    }, [authToken, userInfo]);

    return (
        <section className = { st.profile }>
            <div className = { avatarCX }>
                { loaderCX }
            </div>
            <div className = { st.wrap }>
                <div className = { st.header }>
                    <div>
                        { homeLinkJSX }
                    </div>
                    <div className = {
                        `${stUser.avatar} ${userInfo?.sex === Sex.FEMALE ? stUser.female : stUser.male}`
                    }>
                        <div className = { stUser.column }>
                            <Link
                                to = { book.profile.url }
                                className = { stUser.name }>
                                { `${userInfo?.fname} ${userInfo?.lname}` }
                            </Link>
                            <button
                                onClick = { logOutHandler }
                                className = { stUser.logout }> Выйти
                            </button>
                        </div>
                    </div>
                </div>
                <div className = { contentCX }>
                    { children }
                    { widgetJSX }
                </div>
            </div>
        </section>
    );
};

interface IPropTypes {
    children: ReactElement | ReactElement[];
    center?: boolean;
    disabledWidget?: boolean;
}
