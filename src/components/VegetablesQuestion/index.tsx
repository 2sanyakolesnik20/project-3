// core
import { FC } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';
import { useAppSelector } from '../../lib/redux/init/store';

// components
import { QuestionsInput } from '../../elements/customInput';

// selectors
import { vegetablesActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionSelector/styles/index.module.scss';

export const VegetablesQuestions: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const vegetablesActivity = useAppSelector(vegetablesActivitySelector);
    const { submit }  = useSubmit({ handleSubmit, activity: vegetablesActivity });

    useGetActivityByType({ activity: vegetablesActivity, types: RecordKinds.VEGETABLES });
    useResetForm({ activity: vegetablesActivity, reset });

    return (
        <form onSubmit = { submit } className = { st.question }>
            <h1>Ты сегодня кушал овощи?</h1>
            <div className = { st.answers }>
                <QuestionsInput
                    checkedValue = { vegetablesActivity?.value }
                    htmlFor = 'true'
                    id = 'true'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('vegetables') }
                    value = 'true'
                    label = 'Да' />
                <QuestionsInput
                    checkedValue = { vegetablesActivity?.value }
                    htmlFor = 'false'
                    id = 'false'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('vegetables') }
                    value = 'false'
                    label = 'Нет' />
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};

