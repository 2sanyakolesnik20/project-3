/* eslint-disable no-template-curly-in-string */
// core
import * as yup from 'yup';

// types
import { ProfileFormType } from '../../../types';

const minLength = 'Минимальная длина ${min}';
const maxLength = 'Максимальная длина ${max}';

export const schema: yup.SchemaOf<ProfileFormType> = yup.object().shape({
    fname: yup.string().required('Введите Ваше имя').min(3, minLength).max(12, maxLength),
    lname: yup.string().required('Введите Вашу фамилию').min(3, minLength).max(12, maxLength),
    email: yup.string().required(
        'Электропочта об\'язательна к заполнению',
    ).email('Введите корректную электронную почту'),
    password: yup.string().required('Введите пожалуйста пароль').min(8, minLength).max(12, maxLength),
    age:      yup.number().required().typeError('Введите Ваш возраст').min(
        18, 'Минимальный возраст для регистрации - 18 лет',
    )
        .max(
            80, 'Вам не желательно пользоваться этим приложениям',
        ),
    sex:    yup.string().required().typeError('Выберите Ваш пол'),
    height: yup.number().required().typeError('Введите Ваш рост'),
    weight: yup.number().required().typeError('Введите Ваш вес').min(
        40, 'Минимальный вес 40 кг',
    ),
});
