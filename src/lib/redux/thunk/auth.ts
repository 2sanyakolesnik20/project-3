// core
import { NavigateFunction } from 'react-router-dom';
import { AppThunk } from '../init/store';

// thunks
import { getProfileRequestThunk } from './profile';

// actions
import { setAuthToken, setIsFetching } from '../slices';

// api
import { api } from '../../../api';

// types
import {
    LoginRequestType, ProfileRequestType, ToastOptions, TokenResponseType,
} from '../../../types';

// helpers
import { book } from '../../../navigation/book';
import { createToasters } from '../../../helpers';
import { toastMessages } from '../../../utils';
import { toastConstants } from '../../../constants';

type AuthRequestThunkType<T> = {
    payload: T
    navigate: NavigateFunction
};

export const signUpRequestThunk = (
    { payload, navigate }: AuthRequestThunkType<ProfileRequestType>,
): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        const response = await api.users.create(payload);

        if (response) {
            await dispatch(setAuthToken(response));
            localStorage.setItem('token', response);
            await dispatch(getProfileRequestThunk(response));
            navigate(book.root.url);
        }
    } finally {
        dispatch(setIsFetching(false));
    }
};

export const loginRequestThunk = (
    { payload, navigate }: AuthRequestThunkType<LoginRequestType>,
): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        const response = await api.users.login(payload);

        if (response) {
            await dispatch(setAuthToken(response));
            localStorage.setItem('token', response);
            await dispatch(getProfileRequestThunk(response));
            navigate(book.root.url);
        }
    } finally {
        dispatch(setIsFetching(false));
    }
};

export const logOutRequestThunk = (authToken: TokenResponseType): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        await api.users.logout(authToken);
        createToasters(ToastOptions.info, toastMessages({
            name: toastConstants.USER_LOGOUT,
        }));
    } finally {
        dispatch(setIsFetching(false));
    }
};

