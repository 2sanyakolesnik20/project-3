/* eslint-disable node/no-missing-import */
/* eslint-disable import/no-unresolved */
// core
import { UseFormRegisterReturn } from 'react-hook-form/dist/types/form';

// styles
import st from '../../bus/user/components/login/styles/index.module.scss';

// types
import { ValueType } from '../../types';

type InputPropsType = {
    tag?: string;
    type?: string;
    register: UseFormRegisterReturn;
    label?: string;
    error?: {
        message?: string;
    };
    value?: string | number
    className?: string
    id?: string
    htmlFor?: string
    checkedValue?: ValueType
};

export const GenderInput: React.FC<InputPropsType> = ({
    type, register, label, error, value, className, id, htmlFor, checkedValue,
}) => {
    const input = (
        <input
            type = { type }
            { ...register }
            defaultChecked = { checkedValue === value }
            value = { value }
            id = { id } />
    );

    return (
        <>
            { input }
            <label htmlFor = { htmlFor } className = { className }>
                { label }
            </label>
            <p className = { `${st.error} ${st.genderError}` } >{ error?.message }</p>
        </>
    );
};

GenderInput.defaultProps = {
    type: 'text',
    tag:  'input',
};
