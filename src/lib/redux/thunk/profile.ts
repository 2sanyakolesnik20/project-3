// core
import { AppThunk } from '../init/store';

// actions
import { getUserProfileInfo, setIsFetching } from '../slices';

// types
import { ProfileFormType, ToastOptions, TokenResponseType } from '../../../types';

// api
import { api } from '../../../api';

// helpers
import { createToasters } from '../../../helpers';
import { toastMessages } from '../../../utils';
import { toastConstants } from '../../../constants';

export const getProfileRequestThunk = (token: TokenResponseType): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        const response = await api.users.getMe(token);

        if (response) {
            dispatch(getUserProfileInfo(response));
            createToasters(ToastOptions.success, toastMessages({
                name:    toastConstants.SUCCESS_AUTHORIZATION,
                payload: `${response.fname} ${response.lname}`,
            }));
        }
    } finally {
        dispatch(setIsFetching(false));
    }
};

type UpdateProfileType = {
    payload: ProfileFormType,
    token: TokenResponseType
};

export const updateProfileRequestThunk = (
    { payload, token }: UpdateProfileType,
): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        const response = await api.users.updateMe(payload, token);

        if (response) {
            dispatch(getUserProfileInfo(response.data));
            createToasters(ToastOptions.success, toastMessages({
                name: toastConstants.UPDATE_PROFILE_DATA,
            }));
        }
    } finally {
        dispatch(setIsFetching(false));
    }
};
