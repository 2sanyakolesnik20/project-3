/* eslint-disable no-param-reassign */
// core
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import { TokenResponseType } from '../../../types';

export type InitialAuthSliceStateType = {
    token: TokenResponseType
};

const initialState: InitialAuthSliceStateType = {
    token: null,
};

const authSlice = createSlice({
    name:     'auth',
    initialState,
    reducers: {
        setAuthToken: (state, action: PayloadAction<TokenResponseType>) => {
            state.token = action.payload;
        },
    },
});

export const { setAuthToken } = authSlice.actions;
export default authSlice.reducer;
