// Core
import { FC, ReactElement } from 'react';

// Views
import { Base } from '../../views/base';

type PropTypes = {
    children: ReactElement | ReactElement[]
};

export const Profile: FC<PropTypes> = ({ children }) => {
    return (
        <>
            <Base disabledWidget = { true } center = { true }>
                { children }
            </Base>
        </>
    );
};
