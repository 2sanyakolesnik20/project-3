// Core
import axios, { AxiosResponse } from 'axios';

// Config
import { root } from './config';

// Types
import {
    LoginRequestType,
    ProfileFormType,
    ProfileRequestType,
    ProfileResponseType,
    TokenResponseType,
} from '../types';

export const users = {
    getMe: async (token: TokenResponseType): Promise<ProfileResponseType> => {
        if (!token) {
            throw new Error('токен не указан');
        }

        const { data } = await axios.get<ProfileResponseType>(`${root}/profile`, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        });

        return data;
    },
    create: async (payload: ProfileRequestType): Promise<string> => {
        const { data } = await axios.post<AxiosResponse<string>>(`${root}/users`, payload);

        return data.data;
    },
    updateMe: async (payload: ProfileFormType, token: TokenResponseType) => {
        if (!token) {
            throw new Error('токен не указан');
        }

        const { data } = await axios.put<AxiosResponse<ProfileResponseType>>(`${root}/users`, payload, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        });

        return data;
    },
    login: async (credentials: LoginRequestType): Promise<string> => {
        const { email, password } = credentials;
        const { data } = await axios.get<AxiosResponse<string>>(`${root}/login`, {
            headers: {
                Authorization: `Basic ${window.btoa(`${email}:${password}`)}`,
            },
        });

        return data?.data;
    },
    logout: async (token: TokenResponseType): Promise<void> => {
        if (!token) {
            throw new Error('токен не указан');
        }

        await axios.get(`${root}/logout`, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        });
    },
};
