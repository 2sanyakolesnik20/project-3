/* eslint-disable node/no-missing-import */
/* eslint-disable import/no-unresolved */
// core
import { UseFormRegisterReturn } from 'react-hook-form/dist/types/form';

// styles
import st from '../../bus/user/components/login/styles/index.module.scss';

type InputPropsType = {
    tag?: string;
    type?: string;
    placeholder?: string;
    register: UseFormRegisterReturn;
    label?: string;
    error?: {
        message?: string;
    };
    classNameLabel?: string
    defaultValue?: string | number
    value?: string | number
    className?: string
    id?: string
    htmlFor?: string
    keyProp?: string | number;
};

export const FormsInput: React.FC<InputPropsType> = ({
    type, placeholder, register, label, error, classNameLabel, defaultValue, value, id, htmlFor, keyProp,
}) => {
    const input = (
        <input
            type = { type }
            defaultValue = { defaultValue }
            placeholder = { placeholder }
            { ...register }
            value = { value }
            id = { id } />
    );

    return (
        <>
            <div key = { keyProp } className = { classNameLabel }>
                <label htmlFor = { htmlFor }>
                    { label }
                </label>
                { input }
            </div>
            <p className = { st.error }>{ error?.message }</p>
        </>
    );
};

FormsInput.defaultProps = {
    type: 'text',
    tag:  'input',
};
