// core
import { FC } from 'react';

// components
import { LoginForm } from '../../components';

// styles
import st from '../../bus/user/components/login/styles/index.module.scss';

export const Login: FC = () => {
    return (
        <section className = { st.login }>
            <LoginForm />
        </section>
    );
};

