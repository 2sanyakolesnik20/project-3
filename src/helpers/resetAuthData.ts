// core
import { NavigateFunction } from 'react-router-dom';
import { AppDispatch } from '../lib/redux/init/store';

// actions
import { setAuthToken } from '../lib/redux/slices';

// helpers
import { book } from '../navigation/book';

type ResetAuthDataTypes = {
    dispatch: AppDispatch
    navigate: NavigateFunction
};

export const resetAuthData = ({ dispatch, navigate }: ResetAuthDataTypes): void => {
    localStorage.removeItem('token');
    dispatch(setAuthToken(null));
    navigate(book.login.url);
};
