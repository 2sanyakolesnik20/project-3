// Core
import { FC, ReactElement, useEffect } from 'react';

// hooks
import { useAppDispatch, useAppSelector } from '../../lib/redux/init/store';

// Views
import { Base } from '../../views/base';

// selectors && thunks
import { getCurrentActivityScoreThunk } from '../../lib/redux/thunk';
import { authTokenSelector, currentActivityScoreSelector  } from '../../lib/redux/selectors';

type PropTypes = {
    children: ReactElement | ReactElement[]
};

export const Dashboard: FC<PropTypes> = ({ children }) => {
    const dispatch = useAppDispatch();
    const currentActivityScore = useAppSelector(currentActivityScoreSelector);
    const authToken = useAppSelector(authTokenSelector);

    useEffect(() => {
        if (authToken && !currentActivityScore && currentActivityScore !== 0) {
            dispatch(getCurrentActivityScoreThunk(authToken));
        }
    }, [authToken]);

    return (
        <>
            <Base>
                { children }
            </Base>
        </>
    );
};
