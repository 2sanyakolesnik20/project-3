// Components
import {
    BreakFastQuestion,
    QuestionsCards,
    LunchQuestion,
    DinnerQuestion,
    StepsQuestions,
    FruitsQuestions,
    VegetablesQuestions,
    JunkQuestions,
    WaterQuestions,
    SleepQuestions,
    CoffeeQuestions,
    ProfileForm,
} from '../components';
import {
    Dashboard, Login, Profile, SignUp,
} from '../pages';

const base = '/';

export const book = {
    root: {
        url:  `${base}`,
        page: () => (
            <Dashboard>
                <QuestionsCards />
            </Dashboard>
        ),
    },
    login: {
        url:  `${base}login`,
        page: () => <Login />,
    },
    registration: {
        url:  `${base}registration`,
        page: () => <SignUp />,
    },
    profile: {
        url:  `${base}profile`,
        page: () => (
            <Profile>
                <ProfileForm />
            </Profile>
        ),
    },
    breakfast: {
        url:  `${base}breakfast`,
        page: () => (
            <Dashboard>
                <BreakFastQuestion />
            </Dashboard>
        ),
    },
    coffee: {
        url:  `${base}coffee`,
        page: () => (
            <Dashboard>
                <CoffeeQuestions />
            </Dashboard>
        ),
    },
    dinner: {
        url:  `${base}dinner`,
        page: () => (
            <Dashboard>
                <DinnerQuestion />
            </Dashboard>
        ),
    },
    fruits: {
        url:  `${base}fruits`,
        page: () => (
            <Dashboard>
                <FruitsQuestions />
            </Dashboard>
        ),
    },
    junk: {
        url:  `${base}junk`,
        page: () => (
            <Dashboard>
                <JunkQuestions />
            </Dashboard>
        ),
    },
    lunch: {
        url:  `${base}lunch`,
        page: () => (
            <Dashboard>
                <LunchQuestion />
            </Dashboard>
        ),
    },
    sleep: {
        url:  `${base}sleep`,
        page: () => (
            <Dashboard>
                <SleepQuestions />
            </Dashboard>
        ),
    },
    steps: {
        url:  `${base}steps`,
        page: () => (
            <Dashboard>
                <StepsQuestions />
            </Dashboard>
        ),
    },
    vegetables: {
        url:  `${base}vegetables`,
        page: () => (
            <Dashboard>
                <VegetablesQuestions />
            </Dashboard>
        ),
    },
    water: {
        url:  `${base}water`,
        page: () => (
            <Dashboard>
                <WaterQuestions />
            </Dashboard>
        ),
    },
} as const;

export type BookType = typeof book;
