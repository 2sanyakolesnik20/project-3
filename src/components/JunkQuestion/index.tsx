// core
import { FC } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';
import { useAppSelector } from '../../lib/redux/init/store';

// components
import { QuestionsInput } from '../../elements/customInput';

// selectors
import { junkActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionSelector/styles/index.module.scss';

export const JunkQuestions: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const junkActivity = useAppSelector(junkActivitySelector);
    const { submit }  = useSubmit({ handleSubmit, activity: junkActivity });

    useGetActivityByType({ activity: junkActivity, types: RecordKinds.JUNK });
    useResetForm({ activity: junkActivity, reset });

    return (
        <form onSubmit = { submit } className = { st.question }>
            <h1>Ты сегодня кушал Фастфуд?</h1>
            <div className = { st.answers }>
                <QuestionsInput
                    checkedValue = { junkActivity?.value }
                    htmlFor = 'true'
                    id = 'true'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('junk') }
                    value = 'true'
                    label = 'Да' />
                <QuestionsInput
                    checkedValue = { junkActivity?.value }
                    htmlFor = 'false'
                    id = 'false'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('junk') }
                    value = 'false'
                    label = 'Нет' />
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};

