// core
import { FC, useEffect } from 'react';
import { yupResolver } from '@hookform/resolvers/yup';

// hooks
import { useForm } from 'react-hook-form';
import { useAppDispatch, useAppSelector } from '../../../lib/redux/init/store';

// components
import { FormsInput, GenderInput } from '../../../elements/customInput';

// thunks
import { updateProfileRequestThunk, removeActivityScoreThunk } from '../../../lib/redux/thunk';

// actions
import { resetAllActivities } from '../../../lib/redux/slices';

// selectors
import { authTokenSelector, userProfileInfoSelector } from '../../../lib/redux/selectors';

// types
import { ProfileFormType, Sex } from '../../../types';

// styles
import st from '../../../bus/user/components/profile/styles/index.module.scss';

// schema
import { schema } from './config';

export const ProfileForm: FC = () => {
    const dispatch = useAppDispatch();
    const token = useAppSelector(authTokenSelector);
    const userProfileInfo = useAppSelector(userProfileInfoSelector);

    const {
        register, handleSubmit, formState, reset,
    } = useForm<ProfileFormType>({
        mode:     'onChange',
        resolver: yupResolver(schema),
    });

    const submit = handleSubmit(async (payload: ProfileFormType) => {
        await dispatch(updateProfileRequestThunk({ payload, token }));
        reset();
    });

    const resetActivityScoreHandler = () => {
        dispatch(removeActivityScoreThunk(token));
        dispatch(resetAllActivities());
    };

    useEffect(() => {
        if (userProfileInfo) {
            reset();
        }
    }, [userProfileInfo]);

    return (
        <form
            onSubmit = { submit }
            className = { st.profile }>
            <h1>Профиль</h1>
            <div className = { st.gender }>
                <GenderInput
                    htmlFor = { Sex.MALE }
                    id = { Sex.MALE }
                    checkedValue = { userProfileInfo?.sex }
                    className = { st.male }
                    label =  { 'Мужчина' }
                    register = { register('sex') }
                    value = { Sex.MALE }
                    type = 'radio' />
                <GenderInput
                    htmlFor = { Sex.FEMALE }
                    id = { Sex.FEMALE }
                    checkedValue = { userProfileInfo?.sex }
                    className = { st.female }
                    label =  { 'Женщина' }
                    register = { register('sex') }
                    value = { Sex.FEMALE }
                    type = 'radio' />
            </div>
            <FormsInput
                htmlFor = 'email'
                id = 'email'
                classNameLabel = { st.inputRow }
                keyProp = { userProfileInfo?.email }
                defaultValue = { userProfileInfo?.email }
                label = 'Электропочта'
                register = { register('email') }
                type = 'email'
                placeholder = 'Введите свой email' />
            <FormsInput
                htmlFor = 'fname'
                id = 'fname'
                keyProp = { userProfileInfo?.fname }
                defaultValue = { userProfileInfo?.fname }
                classNameLabel = { st.inputRow }
                label = 'Имя'
                register = { register('fname') }
                placeholder = 'Введите свое имя' />
            <FormsInput
                htmlFor = 'lname'
                id = 'lname'
                keyProp = { userProfileInfo?.lname }
                defaultValue = { userProfileInfo?.lname }
                classNameLabel = { st.inputRow }
                label = 'Фамилия'
                register = { register('lname') }
                placeholder = 'Введите свою фамилию' />
            <FormsInput
                htmlFor = 'password'
                id = 'password'
                classNameLabel = { st.inputRow }
                label = 'Пароль'
                type = 'password'
                register = { register('password') }
                placeholder = 'Введите свой пароль' />
            <FormsInput
                htmlFor = 'age'
                id = 'age'
                keyProp = { userProfileInfo?.age }
                defaultValue = { userProfileInfo?.age }
                classNameLabel = { st.inputRow }
                label = 'Возраст'
                type = 'number'
                register = { register('age') }
                placeholder = 'Введите свой возраст' />
            <FormsInput
                htmlFor = 'height'
                id = 'height'
                keyProp = { userProfileInfo?.height }
                defaultValue = { userProfileInfo?.height }
                classNameLabel = { st.inputRow }
                label = 'Рост'
                type = 'number'
                register = { register('height') }
                placeholder = 'Введите свой рост' />
            <FormsInput
                htmlFor = 'weight'
                id = 'weight'
                keyProp = { userProfileInfo?.weight }
                defaultValue = { userProfileInfo?.weight }
                classNameLabel = { st.inputRow }
                label = 'Вес'
                type = 'number'
                register = { register('weight') }
                placeholder = 'Введите свой вес' />
            <div className = { st.controls }>
                <button
                    onClick = { () => reset() }
                    className = { st.clearData }
                    disabled = { !formState.isDirty }> Сбросить
                </button>
                <button  disabled = { !formState.isValid }>Обновить</button>
            </div>
            <button
                type = 'button'
                onClick = { resetActivityScoreHandler }
                className = { st.clearAllRecords }>Очистить все данные
            </button>
        </form>
    );
};

