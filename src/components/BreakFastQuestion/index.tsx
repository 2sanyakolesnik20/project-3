// core
import { FC } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useAppSelector } from '../../lib/redux/init/store';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';

// components
import { QuestionsInput } from '../../elements/customInput';

// selectors
import { breakfastActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionSelector/styles/index.module.scss';

export const BreakFastQuestion: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const breakfastActivity = useAppSelector(breakfastActivitySelector);
    const { submit }  = useSubmit({ handleSubmit, activity: breakfastActivity });

    useGetActivityByType({ activity: breakfastActivity, types: RecordKinds.BREAKFAST });
    useResetForm({ activity: breakfastActivity, reset });

    return (
        <form
            onSubmit = { submit }
            className = { st.question }>
            <h1>Ты сегодня завтракал?</h1>
            <div className = { st.answers }>
                <QuestionsInput
                    checkedValue = { breakfastActivity?.value }
                    htmlFor = 'none'
                    id = 'none'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('breakfast') }
                    value = 'none'
                    label = 'Я не завтракал' />
                <QuestionsInput
                    checkedValue = { breakfastActivity?.value }
                    htmlFor = 'light'
                    id = 'light'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('breakfast') }
                    value = 'light'
                    label = 'У меня был легкий завтрак' />
                <QuestionsInput
                    checkedValue = { breakfastActivity?.value }
                    htmlFor = 'heavy'
                    id = 'heavy'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('breakfast') }
                    value = 'heavy'
                    label = 'Я очень плотно покушал' />
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};

