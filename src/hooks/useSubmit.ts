// core
import { FieldValues, UseFormHandleSubmit } from 'react-hook-form';

// hooks
import { useCreateOrUpdateActivityByType } from './useCreateOrUpdateActivityByType';

// types
import { RecordRequestType, ResponseRecordType } from '../types';

type FuncArgTypes = {
    handleSubmit: UseFormHandleSubmit<FieldValues>
    activity: ResponseRecordType | null
};

export const useSubmit = ({ handleSubmit, activity }: FuncArgTypes)  => {
    const createOrUpdateActivity = useCreateOrUpdateActivityByType();

    const submit = handleSubmit((answer: { [x: string]: string }) => {
        const requestRecord: RecordRequestType = {
            type:   Object.keys(answer)[ 0 ],
            record: Object.values(answer)[ 0 ],
        };
        createOrUpdateActivity({ activity, types: requestRecord });
    });

    return { submit };
};
