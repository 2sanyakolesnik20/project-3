// core
import { FC } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useAppSelector } from '../../lib/redux/init/store';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';

// components
import { QuestionsInput } from '../../elements/customInput';

// selectors
import { coffeeActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionSelector/styles/index.module.scss';

export const CoffeeQuestions: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const coffeeActivity = useAppSelector(coffeeActivitySelector);
    const { submit }  = useSubmit({ handleSubmit, activity: coffeeActivity });

    useGetActivityByType({ activity: coffeeActivity, types: RecordKinds.COFFEE });
    useResetForm({ activity: coffeeActivity, reset });

    return (
        <form onSubmit = { submit } className = { st.question }>
            <h1>Ты сегодня пил кофе?</h1>
            <div className = { st.answers }>
                <QuestionsInput
                    checkedValue = { coffeeActivity?.value }
                    htmlFor = 'none'
                    id = 'none'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('coffee') }
                    value = 'none'
                    label = 'Я не пил совсем' />
                <QuestionsInput
                    checkedValue = { coffeeActivity?.value }
                    htmlFor = 'light'
                    id = 'light'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('coffee') }
                    value = 'light'
                    label = 'Выпил 1 стакан' />
                <QuestionsInput
                    checkedValue = { coffeeActivity?.value }
                    htmlFor = 'heavy'
                    id = 'heavy'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('coffee') }
                    value = 'heavy'
                    label = 'Выпил 2 или больше стаканов' />
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};

