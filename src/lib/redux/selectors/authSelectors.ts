// core
import { RootState } from '../init/store';

export const authTokenSelector = (state: RootState) => state.auth.token;
