// constants
import { toastConstants } from '../constants';

interface IToastMessages {
    name: string;
    payload?: string;
}

type ToastMessageType = (message: IToastMessages) => string;

export const toastMessages: ToastMessageType = (message) => {
    switch (message.name) {
        case toastConstants.SUCCESS_AUTHORIZATION:
            return `Добро пожаловать ${message.payload}`;
        case toastConstants.REMOVE_ACTIVITY_SCORE:
            return 'Ваша активность обнулена';
        case toastConstants.UPDATE_ACTIVITY_SCORE:
            return 'Активность успешно обновлена!!!';
        case toastConstants.CREATE_ACTIVITY_SCORE:
            return 'Поздравляем, активность добавлена!!!';
        case toastConstants.USER_LOGOUT:
            return 'Заходите еще и помните, что здоровье - превыше всего!!!';
        case toastConstants.UPDATE_PROFILE_DATA:
            return 'Ваши данные успешно обновлены!!!';
        default:
            return 'Не предусмотренное сообщение, но все равно поздравляем!';
    }
};
