// core
import { FC } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import { yupResolver } from '@hookform/resolvers/yup';

// hooks
import { useForm } from 'react-hook-form';
import { useAppDispatch } from '../../../lib/redux/init/store';

// components
import { FormsInput, GenderInput } from '../../../elements/customInput';

// thunks
import { signUpRequestThunk } from '../../../lib/redux/thunk';

// types
import { ProfileFormType, Sex } from '../../../types';

// styles
import st from '../../../bus/user/components/profile/styles/index.module.scss';

// helpers
import { book } from '../../../navigation/book';

// schema
import { schema } from './config';

export const SignUpForm: FC = () => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    const {
        register, handleSubmit, formState, reset,
    } = useForm<ProfileFormType>({
        mode:     'onChange',
        resolver: yupResolver(schema),
    });

    const submit = handleSubmit((payload: ProfileFormType) => {
        dispatch(signUpRequestThunk({ payload, navigate }));
    });

    return (
        <form onSubmit = { submit } className = { st.profile }>
            <h1>Профиль</h1>
            <div className = { st.gender }>
                <GenderInput
                    htmlFor = { Sex.FEMALE }
                    id = { Sex.FEMALE }
                    className = { st.female }
                    label = 'Женщина'
                    register = { register('sex') }
                    value = { Sex.FEMALE }
                    error = { formState.errors.sex }
                    type = 'radio' />
                <GenderInput
                    htmlFor = { Sex.MALE }
                    id = { Sex.MALE }
                    className = { st.male }
                    label = 'Мужчина'
                    register = { register('sex') }
                    value = { Sex.MALE }
                    type = 'radio' />
            </div>
            <FormsInput
                htmlFor = 'email'
                id = 'email'
                classNameLabel = { st.inputRow }
                label = 'Электропочта'
                register = { register('email') }
                type = 'email'
                error = { formState.errors.email }
                placeholder = 'Введите свой email' />
            <FormsInput
                htmlFor = 'fname'
                id = 'fname'
                classNameLabel = { st.inputRow }
                label = 'Имя'
                register = { register('fname') }
                error = { formState.errors.fname }
                placeholder = 'Введите свое имя' />
            <FormsInput
                htmlFor = 'lname'
                id = 'lname'
                classNameLabel = { st.inputRow }
                label = 'Фамилия'
                register = { register('lname') }
                error = { formState.errors.lname }
                placeholder = 'Введите свою фамилию' />
            <FormsInput
                htmlFor = 'password'
                id = 'password'
                classNameLabel = { st.inputRow }
                label = 'Пароль'
                type = 'password'
                register = { register('password') }
                error = { formState.errors.password }
                placeholder = 'Введите свой пароль' />
            <FormsInput
                htmlFor = 'age'
                id = 'age'
                classNameLabel = { st.inputRow }
                label = 'Возраст'
                type = 'number'
                register = { register('age') }
                error = { formState.errors.age }
                placeholder = 'Введите свой возраст' />
            <FormsInput
                htmlFor = 'height'
                id = 'height'
                classNameLabel = { st.inputRow }
                label = 'Рост'
                type = 'number'
                register = { register('height') }
                error = { formState.errors.height }
                placeholder = 'Введите свой рост' />
            <FormsInput
                htmlFor = 'weight'
                id = 'weight'
                classNameLabel = { st.inputRow }
                label = 'Вес'
                type = 'number'
                register = { register('weight') }
                error = { formState.errors.weight }
                placeholder = 'Введите свой вес' />
            <div className = { st.controls }>
                <button
                    className = { st.clearData }
                    disabled = { !formState.isDirty }
                    onClick = { () => reset() }>Сбросить</button>
                <button>Зарегистрироваться</button>
            </div>
            <div className = { st.logIn }>
                <span>Если у вас есть аккаунта, пожалуйста </span>
                <Link to = { book.login.url }> войдите</Link>
            </div>
        </form>
    );
};

