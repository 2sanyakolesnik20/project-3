// hooks
import { useEffect } from 'react';

// types
import { FieldValues, UseFormReset } from 'react-hook-form';
import { ResponseRecordType } from '../types';

type FuncArgTypes = {
    activity: ResponseRecordType | null
    reset:  UseFormReset<FieldValues>
};

export const useResetForm = ({ activity, reset }: FuncArgTypes): void => {
    useEffect(() => {
        if (activity?.value) {
            reset();
        }
    }, [activity]);
};
