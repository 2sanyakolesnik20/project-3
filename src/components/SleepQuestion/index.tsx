// core
import { FC } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';
import { useAppSelector } from '../../lib/redux/init/store';

// selectors
import { sleepActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionInput/styles/index.module.scss';

export const SleepQuestions: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const sleepActivity = useAppSelector(sleepActivitySelector);
    const { submit }  = useSubmit({ handleSubmit, activity: sleepActivity });

    useGetActivityByType({ activity: sleepActivity, types: RecordKinds.SLEEP });
    useResetForm({ activity: sleepActivity, reset });

    return (
        <form onSubmit = { submit } className = { st.question }>
            <h1>Сколько часов ты сегодня спал?</h1>
            <div className = { st.inputRow }>
                <input
                    defaultValue = { typeof sleepActivity?.value === 'number' ? sleepActivity?.value : 0 }
                    type = 'number'
                    placeholder = 'Введите свое число'
                    { ...register('sleep') } />
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};

