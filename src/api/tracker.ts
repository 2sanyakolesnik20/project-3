// Core
import axios, { AxiosResponse } from 'axios';

// Config
import { root } from './config';

// Types
import {
    RecordRequestType,
    ResponseRecordType,
    TokenResponseType,
} from '../types';

export const tracker = {
    getScore: async (token: TokenResponseType) => {
        if (!token) {
            throw new Error('токен не указан');
        }

        const { data } = await axios.get<AxiosResponse<number>>(`${root}/reports`, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        });

        return data.data;
    },
    getRecord: async (type: string, token: TokenResponseType) => {
        if (!token) {
            throw new Error('токен не указан');
        }

        const params = new URLSearchParams({
            kind: type,
        });

        const { data } =  await axios.get<ResponseRecordType>(`${root}/records?${params}`, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        });

        return data;
    },
    createRecord: async ({ type, record }: RecordRequestType, token: TokenResponseType) => {
        if (!token) {
            throw new Error('токен не указан');
        }

        const params = new URLSearchParams({
            kind: type,
        });

        const { data } = await axios.post(`${root}/records?${params}`, {
            value: record,
        }, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        });

        return data;
    },
    updateRecord: async ({ type, record }: RecordRequestType, hash: string, token: TokenResponseType) => {
        if (!token) {
            throw new Error('токен не указан');
        }

        const params = new URLSearchParams({
            kind: type,
        });

        const { data } = await axios.put(`${root}/records/${hash}/?${params}`, {
            value: record,
        }, {
            headers: {
                authorization: `Bearer ${token}`,
            },
        });

        return data;
    },
    removeAllRecords: async (token: TokenResponseType) => {
        if (!token) {
            throw new Error('токен не указан');
        }

        await axios.delete(`${root}/reports/reset`, {
            method:  'DELETE',
            headers: {
                authorization: `Bearer ${token}`,
            },
        });
    },
};
