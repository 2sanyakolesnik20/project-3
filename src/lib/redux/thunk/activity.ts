// core
import { AppThunk } from '../init/store';

// types
import { RecordRequestType, ToastOptions, TokenResponseType } from '../../../types';

// api
import { api } from '../../../api/index';

// actions
import { setIsFetching, setCurrentActivityScore } from '../slices';

// helpers
import { checkActivityType, createToasters } from '../../../helpers';
import { toastMessages } from '../../../utils';
import { toastConstants } from '../../../constants';

export const getCurrentActivityScoreThunk = (
    authToken: TokenResponseType,
): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        const response = await api.tracker.getScore(authToken);

        if (response || response === 0) {
            dispatch(setCurrentActivityScore(response));
        }
    } finally {
        dispatch(setIsFetching(false));
    }
};

export const removeActivityScoreThunk = (authToken: TokenResponseType): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        await api.tracker.removeAllRecords(authToken);

        dispatch(getCurrentActivityScoreThunk(authToken));
        createToasters(ToastOptions.info, toastMessages({
            name: toastConstants.REMOVE_ACTIVITY_SCORE,
        }));
    } finally {
        dispatch(setIsFetching(false));
    }
};

export const getActivityByTypeThunk = (
    type: string, authToken: TokenResponseType,
): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        const response = await api.tracker.getRecord(type, authToken);

        if (response) {
            checkActivityType(type, response, dispatch);
        }
    } finally {
        dispatch(setIsFetching(false));
    }
};

export const createActivityByTypeThunk = (
    record: RecordRequestType, authToken: TokenResponseType,
): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        const response = await api.tracker.createRecord(record, authToken);

        if (response) {
            checkActivityType(record.type, response, dispatch);
            dispatch(getCurrentActivityScoreThunk(authToken));
            createToasters(ToastOptions.success, toastMessages({
                name: toastConstants.CREATE_ACTIVITY_SCORE,
            }));
        }
    } finally {
        dispatch(setIsFetching(false));
    }
};

export const updateActivityByTypeThunk = (
    record: RecordRequestType, authToken: TokenResponseType, hash: string,
): AppThunk => async (dispatch): Promise<void> => {
    try {
        dispatch(setIsFetching(true));
        const response = await api.tracker.updateRecord(record, hash, authToken);

        if (response) {
            checkActivityType(record.type, response, dispatch);
            dispatch(getCurrentActivityScoreThunk(authToken));
            createToasters(ToastOptions.success, toastMessages({
                name: toastConstants.UPDATE_ACTIVITY_SCORE,
            }));
        }
    } finally {
        dispatch(setIsFetching(false));
    }
};
