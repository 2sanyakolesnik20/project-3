// core
import { FC, useState } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';
import { useAppSelector } from '../../lib/redux/init/store';

// components
import { QuestionsInput } from '../../elements/customInput';

// selectors
import { waterActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionCheckboxes/styles/index.module.scss';

export const WaterQuestions: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const waterActivity = useAppSelector(waterActivitySelector);
    const [mlOfWater, setMlOfWater] = useState(typeof waterActivity?.value === 'number' ? waterActivity.value : 0);
    const { submit }  = useSubmit({ handleSubmit, activity: waterActivity });

    useGetActivityByType({ activity: waterActivity, types: RecordKinds.WATER });
    useResetForm({ activity: waterActivity, reset });

    return (
        <form onSubmit = { submit } className = { st.question }>
            <h1>Сколько воды ты сегодня выпил?</h1>
            <div className = { st.elements }>
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '13'
                    id = '13'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 13 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '12'
                    id = '12'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 12 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '11'
                    id = '11'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 11 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '10'
                    id = '10'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 10 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '9'
                    id = '9'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 9 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '8'
                    id = '8'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 8 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '7'
                    id = '7'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 7 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '6'
                    id = '6'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 6 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '5'
                    id = '5'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 5 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '4'
                    id = '4'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 4 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '3'
                    id = '3'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 3 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '2'
                    id = '2'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 2 } />
                <QuestionsInput
                    checkedValue = { waterActivity?.value }
                    setMlOfWater = { setMlOfWater }
                    htmlFor = '1'
                    id = '1'
                    type = 'radio'
                    classNameLabel = { st.element }
                    register = { register('water') }
                    value = { 1 } />
                <span className = { st.size }>{ mlOfWater * 250 } мл</span>
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};

