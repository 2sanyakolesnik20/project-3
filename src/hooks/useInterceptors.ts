// core
import axios, { AxiosError } from 'axios';

// hooks
import { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { useAppDispatch } from '../lib/redux/init/store';

// actions
import { setErrorMessage } from '../lib/redux/slices';

// helpers
import { resetAuthData } from '../helpers';

// types
type AxiosErrorResponseType = {
    statusCode: number;
    message: string;
    error: string;
};

export const useInterceptors = (): void => {
    const navigate = useNavigate();
    const dispatch = useAppDispatch();

    useEffect((): void => {
        axios.interceptors.response.use(
            undefined,
            (error: AxiosError<AxiosErrorResponseType>): Promise<AxiosError> => {
                if (error.response) {
                    if (error.response?.status === 401 || error.response?.status === 404) {
                        resetAuthData({ dispatch, navigate });
                    }
                    dispatch(setErrorMessage(error.response?.data?.message || null));
                }

                return Promise.reject(error);
            },
        );
    }, []);
};
