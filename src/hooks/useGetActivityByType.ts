// hooks
import { useEffect } from 'react';
import { useAppDispatch, useAppSelector } from '../lib/redux/init/store';

// thunks
import { getActivityByTypeThunk } from '../lib/redux/thunk';

// selectors
import { authTokenSelector } from '../lib/redux/selectors';

// types
import { ResponseRecordType } from '../types';

type ArgTypes = {
    activity: ResponseRecordType | null
    types: string
};

export const useGetActivityByType = ({ activity, types }: ArgTypes): void => {
    const authToken = useAppSelector(authTokenSelector);
    const dispatch = useAppDispatch();

    useEffect(() => {
        if (!activity) {
            dispatch(getActivityByTypeThunk(types, authToken));
        }
    }, []);
};
