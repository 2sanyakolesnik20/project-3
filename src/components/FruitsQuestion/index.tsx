// core
import { FC } from 'react';

// hooks
import { useForm } from 'react-hook-form';
import { useGetActivityByType, useResetForm, useSubmit } from '../../hooks';
import { useAppSelector } from '../../lib/redux/init/store';

// components
import { QuestionsInput } from '../../elements/customInput';

// selectors
import { fruitsActivitySelector } from '../../lib/redux/selectors';

// types
import { RecordKinds } from '../../types';

// styles
import st from '../../elements/customQuestionSelector/styles/index.module.scss';

export const FruitsQuestions: FC = () => {
    const { register, handleSubmit, reset } = useForm();
    const fruitsActivity = useAppSelector(fruitsActivitySelector);
    const { submit }  = useSubmit({ handleSubmit, activity: fruitsActivity });

    useGetActivityByType({ activity: fruitsActivity, types: RecordKinds.FRUITS });
    useResetForm({ activity: fruitsActivity, reset });

    return (
        <form onSubmit = { submit } className = { st.question }>
            <h1>Ты сегодня кушал фрукты?</h1>
            <div className = { st.answers }>
                <QuestionsInput
                    checkedValue = { fruitsActivity?.value }
                    htmlFor = 'true'
                    id = 'true'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('fruits') }
                    value = 'true'
                    label = 'Да' />
                <QuestionsInput
                    checkedValue = { fruitsActivity?.value }
                    htmlFor = 'false'
                    id = 'false'
                    type = 'radio'
                    classNameLabel = { st.answer }
                    register = { register('fruits') }
                    value = 'false'
                    label = 'Нет' />
            </div>
            <button className = { st.sendAnswer }>Ответить</button>
        </form>
    );
};

