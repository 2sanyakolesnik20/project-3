/* eslint-disable no-param-reassign */
// core
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import { ResponseRecordType } from '../../../types';

export type InitialActivitySliceStateType = {
    currentActivityScore: number | null
    breakfastActivity: ResponseRecordType | null
    lunchActivity: ResponseRecordType | null
    dinnerActivity: ResponseRecordType | null
    stepsActivity: ResponseRecordType | null
    fruitsActivity: ResponseRecordType | null
    vegetablesActivity: ResponseRecordType | null
    junkActivity: ResponseRecordType | null
    waterActivity: ResponseRecordType | null
    sleepActivity: ResponseRecordType | null
    coffeeActivity: ResponseRecordType | null
};

const initialState: InitialActivitySliceStateType = {
    currentActivityScore: null,
    breakfastActivity:    null,
    lunchActivity:        null,
    dinnerActivity:       null,
    stepsActivity:        null,
    fruitsActivity:       null,
    vegetablesActivity:   null,
    junkActivity:         null,
    waterActivity:        null,
    sleepActivity:        null,
    coffeeActivity:       null,
};

const activitySlice = createSlice({
    name:     'activity',
    initialState,
    reducers: {
        setCurrentActivityScore: (state, action: PayloadAction<number>) => {
            state.currentActivityScore = action.payload;
        },
        setBreakfastActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.breakfastActivity = action.payload;
        },
        setLunchActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.lunchActivity = action.payload;
        },
        setDinnerActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.dinnerActivity = action.payload;
        },
        setStepsActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.stepsActivity = action.payload;
        },
        setFruitsActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.fruitsActivity = action.payload;
        },
        setVegetablesActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.vegetablesActivity = action.payload;
        },
        setJunkActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.junkActivity = action.payload;
        },
        setWaterActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.waterActivity = action.payload;
        },
        setSleepActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.sleepActivity = action.payload;
        },
        setCoffeeActivity: (state, action: PayloadAction<ResponseRecordType>) => {
            state.coffeeActivity = action.payload;
        },
        resetAllActivities: (state) => {
            state.breakfastActivity = null;
            state.lunchActivity = null;
            state.dinnerActivity = null;
            state.stepsActivity = null;
            state.fruitsActivity = null;
            state.vegetablesActivity =  null;
            state.junkActivity = null;
            state.waterActivity = null;
            state.sleepActivity = null;
            state.coffeeActivity = null;
        },
    },
});


export const {
    setCurrentActivityScore,
    setBreakfastActivity,
    setCoffeeActivity,
    setDinnerActivity,
    setFruitsActivity,
    setJunkActivity,
    setLunchActivity,
    setSleepActivity,
    setStepsActivity,
    setVegetablesActivity,
    setWaterActivity,
    resetAllActivities,
} = activitySlice.actions;
export default activitySlice.reducer;
