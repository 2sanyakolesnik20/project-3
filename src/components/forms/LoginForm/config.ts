/* eslint-disable no-template-curly-in-string */
// core
import * as yup from 'yup';

// types
import { LoginRequestType } from '../../../types';

const minLength = 'Минимальная длина ${min}';
const maxLength = 'Максимальная длина ${max}';

export const schema: yup.SchemaOf<LoginRequestType> = yup.object().shape({
    email:    yup.string().required('Электропочта является обязательной').email('Введите корректную электронную почту'),
    password: yup.string().required('Пароль является обязательным').min(8, minLength).max(12, maxLength),
});
