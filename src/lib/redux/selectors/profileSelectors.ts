// core
import { RootState } from '../init/store';

export const userProfileInfoSelector = (state: RootState) => state.profile.userInfo;
