// core
import { FC } from 'react';
import { Link } from 'react-router-dom';

// styles
import st from '../../bus/tracker/components/dashboard/styles/index.module.scss';

// mock
import { mockQuestionCards } from './mockQuestionCards';

export const QuestionsCards: FC = () => {
    const questionCards = mockQuestionCards();

    const questionCardsJSX = questionCards.map(({
        title, desc, className, url,
    }) => (
        <Link
            key = { title }
            to = { url }
            className = { `${`${st.link} ${className}`}` }>
            <span className = { st.title }>{ title }</span>
            <span className = { st.description }>{ desc }</span>
        </Link>
    ));

    return (
        <div className = { st.dashboard }>
            <div className = { st.navigation }>
                <h1>Как у тебя проходит день?</h1>
                <div className = { st.items }>
                    { questionCardsJSX }
                </div>
            </div>
        </div>
    );
};

